Utility to set maintenance mode for list of computers in Solarwinds Orion monitoring tool using SQL records modification.
Options: sort list of objects alphabetically, set maintenance for specific time or from/until, set comment.

![Orion_Unmanage.PNG](https://bitbucket.org/repo/e4xMKk/images/2870196657-Orion_Unmanage.PNG)