﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Orion Maintenance")> 
<Assembly: AssemblyDescription("BETA")> 
<Assembly: AssemblyCompany("SDO Mokra, Czech Republic")> 
<Assembly: AssemblyProduct("Orion Maintenance")> 
<Assembly: AssemblyCopyright("© 2010 Ales Pacik, HeidelbergCement")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("d9c910b5-8511-40e6-9eb1-3dd6a32a457b")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.2.0.1")> 
<Assembly: AssemblyFileVersion("1.2.0.1")> 
