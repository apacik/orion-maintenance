﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.SDOcommentValue = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Duration = New System.Windows.Forms.NumericUpDown()
        Me.sDone = New System.Windows.Forms.Label()
        Me.sProgress = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.SortButton = New System.Windows.Forms.Button()
        Me.InputBox = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ImportButton = New System.Windows.Forms.Button()
        Me.ClearListButton = New System.Windows.Forms.Button()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.UnmanageButton = New System.Windows.Forms.Button()
        Me.SDOcommentButton = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.DurationCheckbox = New System.Windows.Forms.CheckBox()
        Me.DateFrom = New System.Windows.Forms.DateTimePicker()
        Me.DateUntil = New System.Windows.Forms.DateTimePicker()
        Me.TimeFrom = New System.Windows.Forms.DateTimePicker()
        Me.TimeUntil = New System.Windows.Forms.DateTimePicker()
        Me.SDOcommentCheckBox = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UsedDB = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.RemanageButton = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.Login = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Contact = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.NotifyIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        CType(Me.Duration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SDOcommentValue
        '
        resources.ApplyResources(Me.SDOcommentValue, "SDOcommentValue")
        Me.SDOcommentValue.Name = "SDOcommentValue"
        Me.ToolTip.SetToolTip(Me.SDOcommentValue, resources.GetString("SDOcommentValue.ToolTip"))
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Duration
        '
        resources.ApplyResources(Me.Duration, "Duration")
        Me.Duration.Maximum = New Decimal(New Integer() {120, 0, 0, 0})
        Me.Duration.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.Duration.Name = "Duration"
        Me.ToolTip.SetToolTip(Me.Duration, resources.GetString("Duration.ToolTip"))
        Me.Duration.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'sDone
        '
        resources.ApplyResources(Me.sDone, "sDone")
        Me.sDone.Name = "sDone"
        '
        'sProgress
        '
        resources.ApplyResources(Me.sProgress, "sProgress")
        Me.sProgress.Name = "sProgress"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'ProgressBar
        '
        resources.ApplyResources(Me.ProgressBar, "ProgressBar")
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'SortButton
        '
        resources.ApplyResources(Me.SortButton, "SortButton")
        Me.SortButton.Name = "SortButton"
        Me.SortButton.UseVisualStyleBackColor = True
        '
        'InputBox
        '
        Me.InputBox.AcceptsReturn = True
        Me.InputBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.InputBox, "InputBox")
        Me.InputBox.Name = "InputBox"
        Me.InputBox.Tag = ""
        Me.ToolTip.SetToolTip(Me.InputBox, resources.GetString("InputBox.ToolTip"))
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.SortButton)
        Me.GroupBox4.Controls.Add(Me.InputBox)
        Me.GroupBox4.Controls.Add(Me.ImportButton)
        Me.GroupBox4.Controls.Add(Me.ClearListButton)
        Me.GroupBox4.Cursor = System.Windows.Forms.Cursors.Default
        Me.GroupBox4.FlatStyle = System.Windows.Forms.FlatStyle.System
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'ImportButton
        '
        resources.ApplyResources(Me.ImportButton, "ImportButton")
        Me.ImportButton.Name = "ImportButton"
        Me.ImportButton.UseVisualStyleBackColor = True
        '
        'ClearListButton
        '
        resources.ApplyResources(Me.ClearListButton, "ClearListButton")
        Me.ClearListButton.Name = "ClearListButton"
        Me.ClearListButton.UseVisualStyleBackColor = True
        '
        'LinkLabel1
        '
        resources.ApplyResources(Me.LinkLabel1, "LinkLabel1")
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.TabStop = True
        '
        'LinkLabel2
        '
        resources.ApplyResources(Me.LinkLabel2, "LinkLabel2")
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.TabStop = True
        '
        'UnmanageButton
        '
        resources.ApplyResources(Me.UnmanageButton, "UnmanageButton")
        Me.UnmanageButton.Name = "UnmanageButton"
        Me.UnmanageButton.UseVisualStyleBackColor = True
        '
        'SDOcommentButton
        '
        resources.ApplyResources(Me.SDOcommentButton, "SDOcommentButton")
        Me.SDOcommentButton.Name = "SDOcommentButton"
        Me.SDOcommentButton.UseVisualStyleBackColor = True
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'LinkLabel3
        '
        resources.ApplyResources(Me.LinkLabel3, "LinkLabel3")
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.TabStop = True
        '
        'DurationCheckbox
        '
        resources.ApplyResources(Me.DurationCheckbox, "DurationCheckbox")
        Me.DurationCheckbox.Name = "DurationCheckbox"
        Me.DurationCheckbox.UseVisualStyleBackColor = True
        '
        'DateFrom
        '
        Me.DateFrom.Checked = False
        resources.ApplyResources(Me.DateFrom, "DateFrom")
        Me.DateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateFrom.MaxDate = New Date(2050, 9, 1, 0, 0, 0, 0)
        Me.DateFrom.MinDate = New Date(2010, 9, 1, 0, 0, 0, 0)
        Me.DateFrom.Name = "DateFrom"
        Me.DateFrom.ShowCheckBox = True
        Me.DateFrom.Value = New Date(2010, 9, 3, 0, 0, 0, 0)
        '
        'DateUntil
        '
        Me.DateUntil.Checked = False
        resources.ApplyResources(Me.DateUntil, "DateUntil")
        Me.DateUntil.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateUntil.MaxDate = New Date(2050, 9, 1, 0, 0, 0, 0)
        Me.DateUntil.MinDate = New Date(2010, 9, 1, 0, 0, 0, 0)
        Me.DateUntil.Name = "DateUntil"
        Me.DateUntil.ShowCheckBox = True
        Me.DateUntil.Value = New Date(2010, 9, 8, 0, 0, 0, 0)
        '
        'TimeFrom
        '
        Me.TimeFrom.Checked = False
        resources.ApplyResources(Me.TimeFrom, "TimeFrom")
        Me.TimeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.TimeFrom.MaxDate = New Date(2050, 9, 1, 0, 0, 0, 0)
        Me.TimeFrom.MinDate = New Date(2010, 9, 1, 0, 0, 0, 0)
        Me.TimeFrom.Name = "TimeFrom"
        Me.TimeFrom.ShowUpDown = True
        Me.TimeFrom.Value = New Date(2010, 9, 9, 0, 0, 0, 0)
        '
        'TimeUntil
        '
        Me.TimeUntil.Checked = False
        resources.ApplyResources(Me.TimeUntil, "TimeUntil")
        Me.TimeUntil.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.TimeUntil.MaxDate = New Date(2050, 9, 1, 0, 0, 0, 0)
        Me.TimeUntil.MinDate = New Date(2010, 9, 1, 0, 0, 0, 0)
        Me.TimeUntil.Name = "TimeUntil"
        Me.TimeUntil.ShowUpDown = True
        Me.TimeUntil.Value = New Date(2010, 9, 9, 0, 0, 0, 0)
        '
        'SDOcommentCheckBox
        '
        resources.ApplyResources(Me.SDOcommentCheckBox, "SDOcommentCheckBox")
        Me.SDOcommentCheckBox.Name = "SDOcommentCheckBox"
        Me.SDOcommentCheckBox.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.UsedDB)
        Me.GroupBox1.Controls.Add(Me.LinkLabel3)
        Me.GroupBox1.Controls.Add(Me.LinkLabel2)
        Me.GroupBox1.Controls.Add(Me.LinkLabel1)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'UsedDB
        '
        Me.UsedDB.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.UsedDB, "UsedDB")
        Me.UsedDB.Name = "UsedDB"
        '
        'PictureBox1
        '
        resources.ApplyResources(Me.PictureBox1, "PictureBox1")
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.TabStop = False
        '
        'RemanageButton
        '
        resources.ApplyResources(Me.RemanageButton, "RemanageButton")
        Me.RemanageButton.Name = "RemanageButton"
        Me.RemanageButton.UseVisualStyleBackColor = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.PictureBox2)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.DurationCheckbox)
        Me.GroupBox2.Controls.Add(Me.TimeUntil)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.TimeFrom)
        Me.GroupBox2.Controls.Add(Me.DateUntil)
        Me.GroupBox2.Controls.Add(Me.DateFrom)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Duration)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'PictureBox2
        '
        resources.ApplyResources(Me.PictureBox2, "PictureBox2")
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.TabStop = False
        '
        'ToolTip
        '
        Me.ToolTip.AutoPopDelay = 10000
        Me.ToolTip.InitialDelay = 200
        Me.ToolTip.ReshowDelay = 100
        Me.ToolTip.ShowAlways = True
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Login, Me.Contact})
        resources.ApplyResources(Me.StatusStrip, "StatusStrip")
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode
        Me.StatusStrip.SizingGrip = False
        '
        'Login
        '
        Me.Login.ActiveLinkColor = System.Drawing.Color.Black
        Me.Login.Name = "Login"
        resources.ApplyResources(Me.Login, "Login")
        Me.Login.Spring = True
        '
        'Contact
        '
        Me.Contact.ActiveLinkColor = System.Drawing.Color.Blue
        Me.Contact.IsLink = True
        Me.Contact.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.Contact.LinkColor = System.Drawing.Color.Black
        Me.Contact.Name = "Contact"
        resources.ApplyResources(Me.Contact, "Contact")
        '
        'PictureBox3
        '
        resources.ApplyResources(Me.PictureBox3, "PictureBox3")
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.TabStop = False
        '
        'NotifyIcon
        '
        resources.ApplyResources(Me.NotifyIcon, "NotifyIcon")
        '
        'Form1
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.RemanageButton)
        Me.Controls.Add(Me.SDOcommentCheckBox)
        Me.Controls.Add(Me.SDOcommentButton)
        Me.Controls.Add(Me.UnmanageButton)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.sDone)
        Me.Controls.Add(Me.sProgress)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.SDOcommentValue)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        CType(Me.Duration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SDOcommentValue As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Duration As System.Windows.Forms.NumericUpDown
    Friend WithEvents sDone As System.Windows.Forms.Label
    Friend WithEvents sProgress As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents SortButton As System.Windows.Forms.Button
    Friend WithEvents InputBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ImportButton As System.Windows.Forms.Button
    Friend WithEvents ClearListButton As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents UnmanageButton As System.Windows.Forms.Button
    Friend WithEvents SDOcommentButton As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
    Friend WithEvents DurationCheckbox As System.Windows.Forms.CheckBox
    Friend WithEvents DateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateUntil As System.Windows.Forms.DateTimePicker
    Friend WithEvents TimeFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents TimeUntil As System.Windows.Forms.DateTimePicker
    Friend WithEvents SDOcommentCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RemanageButton As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents Login As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Contact As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents UsedDB As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents NotifyIcon As System.Windows.Forms.NotifyIcon

End Class
