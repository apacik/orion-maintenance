﻿
Public Class Form1

    Dim sText, sTotal As String
    Dim sArray() As String
    Dim i As Integer
    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim conError As Boolean
    Dim userProfile As String
    Dim oldCI As System.Globalization.CultureInfo
    Dim DBServer As String = "DEUSDHEID0053" '"localhost\SQLExpress"

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Initialize before program startup
        NotifyIcon.Visible = False
        oldCI = System.Threading.Thread.CurrentThread.CurrentCulture
        ProgressBar.Minimum = 0
        UsedDB.Text = DBServer
        Login.Text = ("   " & My.User.Name)
        DateFrom.MinDate = Now
        DateFrom.Checked = False
        TimeFrom.Enabled = False
        userProfile = Environment.GetEnvironmentVariable("USERPROFILE")

    End Sub

    Private Sub ClearListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearListButton.Click

        InputBox.Clear()
        ProgressBar.Maximum = 0
        sDone.Text = 0
        sProgress.Text = 0

    End Sub

    Private Sub ImportButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportButton.Click

        Dim AllText As String = ""
        OpenFileDialog.InitialDirectory = (userProfile & "\My Documents")
        OpenFileDialog.Filter = "Text files (*.txt)|*.txt"
        'display Open dialog box
        If OpenFileDialog.ShowDialog() = DialogResult.OK Then
            Try 'open file and trap any errors using handler
                AllText = My.Computer.FileSystem.ReadAllText(OpenFileDialog.FileName)
                InputBox.Text = AllText 'display file
            Catch ex As Exception
                MessageBox.Show("An error occurred." & vbCrLf & ex.Message)
            End Try
        End If

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        System.Diagnostics.Process.Start("http://eu-solarwinds/Orion/")

    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked

        System.Diagnostics.Process.Start("http://eu-solarwinds/Orion/Report.aspx?Report=Information_about_Unmanged_nodes")

    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked

        System.Diagnostics.Process.Start("http://eu-solarwinds/Orion/Report.aspx?Report=Scheduled_unmanage_mode_for_servers_in_SDO_scope")

    End Sub

    Private Sub Contact_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Contact.Click

        System.Diagnostics.Process.Start("mailto:" & "ales.pacik@heidelbergcement.com" & "?subject=Orion Unmanage")

    End Sub


    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateFrom.ValueChanged

        If DateFrom.Checked Then
            TimeFrom.Enabled = True
        Else
            TimeFrom.Enabled = False
        End If

    End Sub

    Private Sub SDOcommentCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SDOcommentCheckBox.CheckedChanged

        If SDOcommentCheckBox.Checked Then
            SDOcommentValue.Enabled = True
            SDOcommentValue.Clear()
        Else
            SDOcommentValue.Enabled = False
        End If

    End Sub

    Private Sub DateUntil_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateUntil.ValueChanged

        If DateUntil.Checked Then
            TimeUntil.Enabled = True
            DurationCheckbox.Enabled = False
            SDOcommentCheckBox.Enabled = True
            DateFrom.Checked = True
            TimeFrom.Enabled = True
        Else
            TimeUntil.Enabled = False
            DurationCheckbox.Enabled = True
            SDOcommentCheckBox.Enabled = False
            DateFrom.Checked = False
            TimeFrom.Enabled = False
        End If

    End Sub

    Private Sub DefinedDuration_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DurationCheckbox.CheckedChanged

        If DurationCheckbox.Checked Then
            Duration.Enabled = True
            DateUntil.Enabled = False
            Duration.Value = 30
            SDOcommentCheckBox.Enabled = True
            If Not DateFrom.Checked = True Then
                DateFrom.Checked = True
                TimeFrom.Enabled = True
                DateFrom.Value = Now
                TimeFrom.Value = Now
            End If
        Else
            Duration.Enabled = False
            DateUntil.Enabled = True
            DateFrom.Checked = False
            TimeFrom.Enabled = False
            SDOcommentCheckBox.Enabled = False
            SDOcommentCheckBox.Checked = False
            SDOcommentValue.Enabled = False
            SDOcommentValue.Clear()
        End If

    End Sub

    Private Sub InputBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InputBox.TextChanged

        sText = InputBox.Text

        'replace different new line characters with one version and remove spaces
        sText = sText.Replace(vbCrLf, vbCr)
        sText = sText.Replace(vbLf, vbCr)
        sText = sText.Replace(" ", vbNullString)
        sText = sText.Replace(vbTab, vbNullString)

        'remove last carriage return if it exists
        If sText.EndsWith(vbCr) Then
            sText = sText.Substring(0, sText.Length - 1)
        End If

        'split each line in to an array
        sArray = sText.Split(vbCr)

        'set progress bar maximum value
        ProgressBar.Maximum = UBound(sArray) + 1

        'number of servers to progresss
        sProgress.Text = UBound(sArray) + 1
        sDone.Text = 0
        sTotal = sProgress.Text

    End Sub

    Private Sub Inputbox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles InputBox.KeyPress

        If e.KeyChar = Convert.ToChar(1) Then
            DirectCast(sender, TextBox).SelectAll()
            e.Handled = True
        End If

    End Sub

    Private Sub SortButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SortButton.Click

        Dim s As Integer

        If InputBox.Text = Nothing Then
            MessageBox.Show("List of servers is empty !", "Sort and trim", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'sort array
        Array.Sort(sArray)

        sText = ""
        For s = 0 To UBound(sArray)
            sText = sText & sArray(s) & vbCrLf
        Next s

        'write sorted text
        InputBox.Text = sText

        'remove text selection
        InputBox.Select(0, 0)

    End Sub

    Private Sub UnmanageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnmanageButton.Click

        Dim SDOComment As String
        Dim sMember As String
        conError = False

        'Check if list of server is not empty
        If InputBox.Text = Nothing Then
            MessageBox.Show("List of servers is empty !", "Unmanage", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Check if unmanage beginning is selected
        If DateFrom.Checked = False Then
            MessageBox.Show("No date selected for unmanage beginning !", "Unmanage", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Check if unmanage until or duration is selected
        If DateFrom.Checked = True And DateUntil.Checked = False And DurationCheckbox.Checked = False Then
            MessageBox.Show("No date for unmanage until or duration selected !", "Unmanage", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'UnManage From and Until definition
        Dim UnFrom As Date = Trim(DateFrom.Text) & " " & TimeFrom.Text
        Dim UnUntil As Date = Trim(DateUntil.Text) & " " & TimeUntil.Text

        'Check if unmanage until is not same as unmanage beginning
        If DateUntil.Checked = True Then
            If UnUntil = UnFrom Then
                MessageBox.Show("Unmanage until cannot be same as beginning !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If

        'Check if unmanage until is not less than 5 minutes from beginning  or in past
        If DateUntil.Checked = True Then
            If UnUntil <= UnFrom.AddMinutes(5) Then
                MessageBox.Show("Unmanage until cannot be in past or less than 5 minutes from beginning !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If

        'Check if sdo comment is not empty
        If SDOcommentCheckBox.Checked = True And SDOcommentValue.Text = Nothing Then
            MessageBox.Show("SDO comment is empty !", "Unmanage", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If DurationCheckbox.Checked = True Then
            UnUntil = UnFrom.AddMinutes(Duration.Text)
            'MessageBox.Show("Hodnota time until: " & UnUntil)
        End If


        Dim UnManageFrom As String = Format(UnFrom, "yyyy-MM-dd HH:mm:ss")
        Dim UnManageUntil As String = Format(UnUntil, "yyyy-MM-dd HH:mm:ss")

        sqlConnect()

        If conError = True Then
            MessageBox.Show("Orion was not updated !", "Error occurred", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Update DB with UnManage time and with/without SDO comment
        If SDOcommentCheckBox.Checked Then

            'Update DB with SDO Comment
            SDOComment = SDOcommentValue.Text
            i = 0
            For Each sMember In sArray
                If sMember <> "" Then
                    cmd.CommandText = "UPDATE dbo.nodes " &
                                      "SET UnManageFrom = '" & UnManageFrom & "', " &
                                      "UnManageUntil = '" & UnManageUntil & "', " &
                                      "SDO_Comment = '" & SDOComment & "' " &
                                      "WHERE caption = '" & sMember & "'"
                    cmd.ExecuteNonQuery()
                    i = i + 1
                    ProgressBar.Value = i
                    sDone.Text = i
                    sDone.Update()
                    sProgress.Text = sTotal - i
                    sProgress.Update()
                End If
            Next

        Else

            i = 0
            For Each sMember In sArray
                If sMember <> "" Then
                    cmd.CommandText = "UPDATE dbo.nodes " &
                                      "SET UnManageFrom = '" & UnManageFrom & "', " &
                                      "UnManageUntil = '" & UnManageUntil & "' " &
                                      "WHERE caption = '" & sMember & "'"
                    cmd.ExecuteNonQuery()
                    i = i + 1
                    ProgressBar.Value = i
                    sDone.Text = i
                    sDone.Update()
                    sProgress.Text = sTotal - i
                    sProgress.Update()
                End If
            Next
        End If
        con.Close()
        sMember = Nothing
        MessageBox.Show("Servers in list are in scheduled unmanage mode now.", "Unmanage", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'Inicialize to default
        iniDefault()

    End Sub

    Private Sub SDOcommentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SDOcommentButton.Click

        Dim sMember As String
        Dim aConfirm As String

        'Check if servers are in list
        If InputBox.Text = Nothing Then
            MessageBox.Show("List of servers is empty !", "Clean SDO comment", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Action confirmation
        aConfirm = MessageBox.Show("Are you sure to delete SDO comment for list of servers ?", "Action verification", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If aConfirm = vbNo Then
            Exit Sub
        End If

        conError = False

        sqlConnect()
        If conError = True Then
            MessageBox.Show("Orion was not updated !", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Update DB with SDO_Comment NULL value
        i = 0
        For Each sMember In sArray
            If sMember <> "" Then
                cmd.CommandText = "UPDATE dbo.nodes " &
                                  "SET SDO_Comment = NULL " &
                                  "WHERE caption = '" & sMember & "'"
                cmd.ExecuteNonQuery()
                i = i + 1
                ProgressBar.Value = i
                sDone.Text = i
                sDone.Update()
                sProgress.Text = sTotal - i
                sProgress.Update()
            End If
        Next
        con.Close()
        sMember = Nothing
        MessageBox.Show("SDO comment is removed for servers in list.", "Clean SDO comment", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'Inicialize to default
        iniDefault()

    End Sub

    Private Sub RemanageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RemanageButton.Click

        Dim sMember As String

        'Check if servers are in list
        If InputBox.Text = Nothing Then
            MessageBox.Show("List of servers is empty !", "Remanage", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        conError = False

        sqlConnect()
        If conError = True Then
            MessageBox.Show("Orion was not updated !", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Update DB to Remanage if there is no error with DB connection
        i = 0
        For Each sMember In sArray
            If sMember <> "" Then
                cmd.CommandText = "UPDATE dbo.nodes " &
                                  "SET UnManageFrom = '1899-12-30 00:00:00', " &
                                  "UnManageUntil = '1899-12-30 00:00:00', " &
                                  "SDO_Comment = NULL " &
                                  "WHERE caption = '" & sMember & "'"
                cmd.ExecuteNonQuery()
                i = i + 1
                ProgressBar.Value = i
                sDone.Text = i
                sDone.Update()
                sProgress.Text = sTotal - i
                sProgress.Update()
            End If
        Next
        con.Close()
        sMember = Nothing
        MessageBox.Show("Servers in list are remanaged now.", "Remanage", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'Inicialize to default
        InputBox.Clear()
        sTotal = 0
        iniDefault()

    End Sub

    Sub iniDefault()

        ProgressBar.Value = 0
        sDone.Text = 0
        sProgress.Text = sTotal
        DateFrom.Checked = False
        DateUntil.Checked = False
        DateFrom.Enabled = True
        DateUntil.Enabled = True
        DurationCheckbox.Checked = False
        SDOcommentCheckBox.Checked = False
        SDOcommentCheckBox.Enabled = True
        DurationCheckbox.Enabled = True

    End Sub

    Sub sqlConnect()

        Try
            con.ConnectionString = "Data Source=" & DBServer & ";" & _
                                   "Initial Catalog=dbSolarWindsNetPerfMon;" & _
                                   "Trusted_Connection=Yes;" & _
                                   "Integrated Security=SSPI;"
            con.Open()
            cmd.Connection = con
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error connecting to database", MessageBoxButtons.OK, MessageBoxIcon.Error)
            conError = True
            con.Close()
        Finally
        End Try

    End Sub

    Private Sub NotifyIcon_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon.MouseClick
        Try
            Me.Show()
            Me.WindowState = FormWindowState.Normal
            NotifyIcon.Visible = False
        Catch ex As Exception
            MessageBox.Show("An error occurred." & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Try
            If Me.WindowState = FormWindowState.Minimized Then
                Me.WindowState = FormWindowState.Minimized
                NotifyIcon.Visible = True
                Me.Hide()
                NotifyIcon.ShowBalloonTip(100, "Orion Unmanage", "Application is minimized..", ToolTipIcon.Info)
            End If
        Catch ex As Exception
            MessageBox.Show("An error occurred." & vbCrLf & ex.Message)
        End Try
    End Sub

End Class